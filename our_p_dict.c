#include "our_p_dict.h"

// Get the length of a file
int len_of_file(FILE *file)
{
	fseek(file, 0, SEEK_END);
	return ftell(file);
}

// Get the number of lines in a file
int lines_in_file(FILE *file)
{
	int lines = 0;
	char c;
	while((c=getc(file)) != EOF)
	{
		if (c == '\n')
		{
			lines++;
		}
	}
	return lines;
}

// Get the number of words in each line of a file
int *num_words_in_lines(FILE *file, int line_num)
{
	int counter = 0;
	int spot_count = 0;
	char c;
	int *lens = (int *)malloc(sizeof(int) * line_num);
	while((c=getc(file)) != EOF)
	{
		if (c == ' ')
		{
			counter++;
		}
		if (c == '\n')
		{
			counter++;
			lens[spot_count] = counter;
			spot_count++;
			counter = 0;
		}
	}
	return lens;
}

// Initialize dictionary
Dict_part **init_dict(int line_num, int *lens)
{
	Dict_part **dictionary;
	dictionary = (Dict_part **)malloc(sizeof(Dict_part *) * line_num);
	for (int i = 0; i < line_num; i++)
	{
		dictionary[i] = (Dict_part *)malloc(sizeof(Dict_part) * lens[i]);
	}
	return dictionary;
}

// Put contents of file into dictionary
void load_phrases(FILE *file, Dict_part **dictionary, int length, int line_num, int *lens)
{
	// Initialize variables
	char *word;
	char *line;
	char word_copy[length];
	char buffer[length];

	// Loop through all the lines in the file
	for (int i = 0; i < line_num; i++)
	{
		Dict_part *part;
		part = (Dict_part *)malloc(sizeof(Dict_part));
		int count = 0;
		char ph[lens[i]][length];
		fgets(buffer, length, file);
		word = strtok(buffer, " ");
		strcpy(ph[count], word);
		count++;

		// Loop through each word in the line
		while(word != NULL)
		{
			word = strtok(NULL, " ");
			if (word != NULL)
			{
				strcpy(ph[count], word);
			}
			count++;
		}

		// Loop through words in line to copy them to Dict_part
		for (int j = 0; j < lens[i]; j++)
		{
			strcpy(part->phrase[j], ph[j]);
		}

		// Loop through words in line to copy them to dictionary
		for (int k = 0; k < lens[i]; k++)
		{
			strcpy(dictionary[i]->phrase[k], part->phrase[k]);
			dictionary[i]->score = 1;
		}

	}
}

// Print dictionary
void print_dict(Dict_part **dictionary, int line_num, int *lens)
{
	for (int i = 0; i < line_num; i++)
	{
		for (int j = 0; j < lens[i]; j++)
		{
			printf("%s ", dictionary[i]->phrase[j]);
		}
		printf("%d\n\n", dictionary[i]->score);
	}
}

// Make dictionary
Dict_part **make_dic(const char *one)
{
	// Open and read the file
	FILE *f;
	f = fopen(one, "r");

	// Get the length of the file
	int len = len_of_file(f);
	fseek(f, 0, SEEK_SET);  // Go back to the start of the file

	// Get the number of lines in the file
	int lines = lines_in_file(f);
	fseek(f, 0, SEEK_SET);  // Go back to the start of the file

	// Get number of words in each line of the file
	int *line_len = num_words_in_lines(f, lines);
	fseek(f, 0, SEEK_SET);  // Go back to the start of the file

	// Initiliaze the dictionary
	Dict_part **phrase_dict = init_dict(lines, line_len);

	// Load the contents of the file into a dictionary
	load_phrases(f, phrase_dict, len, lines, line_len);

	// Print phrase dictionary
	// print_dict(phrase_dict, lines, line_len);

	// Close the file
	fclose(f);
	return phrase_dict;
}
