#include <stdio.h>
#include <stdlib.h> 
#include <string.h> 

typedef struct Dict_parts
{
	char *phrase[1000];
	int score;
} Dict_part;

typedef struct tree
{
	char head[100];
	struct tree *next;
	int len;  // Number of elements in next
} p_tree;

int main()
{
	// Make test array
	Dict_part *one = (Dict_part *)malloc(sizeof(Dict_part));
	one->phrase[0] = "this";
	one->phrase[1] = "is";
	one->phrase[2] = "a";
	one->phrase[3] = "list";
	one->phrase[4] = "of";
	one->phrase[5] = "words";
	one->score = 3;

	Dict_part *two = (Dict_part *)malloc(sizeof(Dict_part));
	two->phrase[0] = "maybe";
	two->phrase[1] = "this";
	two->phrase[2] = "work";
	two->score = 2;

	Dict_part *three = (Dict_part *)malloc(sizeof(Dict_part));
	three->phrase[0] = "maybe";
	three->phrase[1] = "it";
	three->phrase[2] = "does";
	three->score = 2;

	Dict_part *four = (Dict_part *)malloc(sizeof(Dict_part));
	four->phrase[0] = "maybe";
	four->phrase[1] = "it";
	four->phrase[2] = "doesn't";
	four->score = 1;

	Dict_part *five = (Dict_part *)malloc(sizeof(Dict_part));
	five->phrase[0] = "i";
	five->phrase[1] = "guess";
	five->phrase[2] = "we";
	five->phrase[3] = "shall";
	five->phrase[4] = "see";
	five->score = 1;

	Dict_part *six = (Dict_part *)malloc(sizeof(Dict_part));
	six->phrase[0] = "here";
	six->phrase[1] = "goes";
	six->phrase[2] = "nothing";
	six->score = 1;

	Dict_part *arr = (Dict_part *)malloc(sizeof(Dict_part) * 6);
	arr[0] = *one;
	arr[1] = *two;
	arr[2] = *three;
	arr[3] = *four;
	arr[4] = *five;
	arr[5] = *six;
	// End of test array

	int lens[6];
	lens[0] = 6;
	lens[1] = 3;
	lens[2] = 3;
	lens[3] = 3;
	lens[4] = 5;
	lens[5] = 3;

	// for (int i = 0; i < 6; i++)
	// {
	// 	printf("LEN: %d\n", lens[i]);
	// }
	// printf("\n");

	char *heads[6];
	char *prev = arr[0].phrase[0];
	heads[0] = prev;
	int num_heads = 1;

	for (int i = 1; i < 6 /* len of dict */; i++)
	{
		if (arr[i].phrase[0] != prev)
		{
			heads[num_heads] = arr[i].phrase[0];
			num_heads++;
			prev = arr[i].phrase[0];
		}
	}

	// Test creation of heads
	// for (int j = 0; j < num_heads; j++)
	// {
	// 	printf("HEAD%d: %s\n", j, heads[j]);
	// }
	// printf("\n");

	p_tree *trees;
	trees = (p_tree *)malloc(sizeof(p_tree *) * num_heads);
	for (int j = 0; j < num_heads; j++)
	{
		//trees[j] = (p_tree *)malloc(sizeof(p_tree));
		p_tree tree;
	 	strcpy(tree.head, heads[j]);
	 	// printf("TH: %s\n", tree.head);
		strcpy(trees[j].head, tree.head);
		//trees[j].next = malloc(sizeof(p_tree) * 1000);
		trees[j].len = 0;
	}

	// for (int j = 0; j < num_heads; j++)
	// {
	// 	printf("HEAD%d: %s %d\n", j, trees[j].head, trees[j].len);
	// }

	for (int j = 0; j < num_heads; j++)
	{
		for (int k = 0; k < 6 /* len of dict */; k++)
		{
			if (strcmp(arr[k].phrase[0], trees[j].head) == 0)
			{
				if (trees[j].len == 0)
				{
					trees[j].next = malloc(sizeof(p_tree) * 1000);
				}

				int index;
				int match = 0;
				p_tree next_tree;
				for (int m = 0; m < trees[j].len; m++)
				{
					if (strcmp(arr[k].phrase[1], trees[j].next[m].head) == 0)
					{
						index = m;
						match = 1;
					}
				}
				if (!match)
				{
					strcpy(trees[j].next[trees[j].len].head, arr[k].phrase[1]);
					trees[j].len++;
					next_tree = trees[j].next[trees[j].len];
					if (next_tree.len == 0)
					{
						next_tree.next = malloc(sizeof(p_tree) * 1000);
					}
				}
				else
				{
					next_tree = trees[j].next[index];
				}

				// Add to trees
				for (int l = 1; l < lens[k]/* items in phrase */; l++)
				{
					int idx;
					int eq = 0;
					for (int n = 0; n < next_tree.len; n++)
					{
						if (strcmp(arr[k].phrase[l], next_tree.next[n].head) == 0)
						{
							idx = n;
							eq = 1;
						}
					}
					if (!eq)
					{
						strcpy(next_tree.next[next_tree.len].head, arr[k].phrase[l]);
						next_tree.len++;
						next_tree = next_tree.next[next_tree.len];
						if (next_tree.len == 0)
						{
							next_tree.next = malloc(sizeof(p_tree) * 1000);
						}
					}
					else
					{
						next_tree = next_tree.next[idx];
					}
				}
			}
		}
	}
	for (int j = 0; j < num_heads; j++)
	{
		printf("\nNEXT HEAD%d: %s %d\n", j, trees[j].next[0].head, trees[j].len);
	}

    return 0; 
}