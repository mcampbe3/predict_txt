#include <stdio.h>
#include <ncurses.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int kbhit(void)
{
    int ch = getch();

    if (ch != ERR) {
        ungetch(ch);
        return 1;
    } else {
        return 0;
    }
}

int main()
{
    initscr();

    cbreak();
    noecho();
    nodelay(stdscr, TRUE);

    scrollok(stdscr, TRUE);
    int count = 1;
    char word[1000];
    char sentence[100][100];
    int long_count = 0;
    while (1) {
        if (kbhit()) {
          char c = getch();
          char option1[100];
          char option2[100];
          // char options[2][100];
          // char *option1 = (char *)malloc(sizeof(char) * 100);
          // char *option2 = (char *)malloc(sizeof(char) * 100);
          if (c != '1' && c != '2' && c != '3' && c != '/')
          {
            word[long_count] = c;
            printw("Adding new character...%s %d %c %c\n", word, long_count, c, word[long_count]);
          }
          if (c == '\n')
          {
            return 0;
          }
          if (c == ' ')
          {
            word[long_count] = c;
            char options[2][100] = {"next", "word"}; 
            strcpy(option1, options[0]);
            strcat(option1, " ");
            strcpy(option2, options[1]);
            strcat(option2, " ");
          }
          else
          {
            char options[2][100] = {"hey", "heyo"};  
            strcpy(option1, options[0]);
            strcat(option1, " ");
            strcpy(option2, options[1]);
            strcat(option2, " ");
          }
          if (strlen(word) != 0)
          {
            printw("\nOptions ->\t1 : %s \t2 : %s\n\n", option1, option2);
          }
          if (c == '1')
          {
            long_count = long_count - count;
            int lc = long_count;
            for (int j = 0; j < count + 1; j++)
            {
              word[long_count] = '\0';
              long_count++;
            }
            strcat(word, option1);
            if (count + 1 - strlen(option1) > 0)
            {
              for (int j = long_count; j < long_count + count + 2 - strlen(option1); j++)
              {
                word[j] = '\0';
              }
            }
            printw("\r ");
          }
          if (c == '2')
          {
            long_count = long_count - count;
            int lc = long_count;
            for (int j = 0; j < count + 1; j++)
            {
              word[long_count] = '\0';
              long_count++;
            }
            strcat(word, option2);
            if (count + 1 - strlen(option2) > 0)
            {
              for (int j = long_count; j < long_count + count + 2 - strlen(option2); j++)
              {
                word[j] = '\0';
              }
            }
            printw("\r ");
          }
          if (c == '/')
          {
            long_count--;
            word[long_count] = '\0';
            long_count--;
            count--;
            printw("\r ");
          }
          printw("%s", word);
          refresh();
          count++;
          long_count++;
          if (c == ' ')
          {
            count = 0;
          }
        }
        else {
            refresh();
            sleep(1);
        }
    }
}