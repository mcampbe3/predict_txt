#ifndef OUR_W_DICT_H_
#define OUR_W_DICT_H_
#ifndef DEBUG
#define DEBUG 0
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "ex_w.c"

#define our_file "our_w.txt"

// Create a word type with score one given a string
word make_word(char *x);

// Update size
void addonesize(word *before, int *count);

// If a word doesn't exist then add the word to the final array
word *add_new_word(word *final_arr, int *count, char *x);

// Given our final array and the # of items in that array, either find the same word
// add a score of 1 to its score, or if it's not found then add it to the final array and sort it
void add_score(word *final_arr, int *count, char *x);

#endif
