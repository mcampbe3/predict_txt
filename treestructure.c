#include <stdio.h>
#include <stdlib.h> 
#include <string.h> 

#include "our_w_dict.c"

// turns current character into index from a - z
#define char_ind(b) ((int)b - (int)'a')
// this is the tree structure file

struct tree
{
	//point to all the alphabet (NULL if nothing is there)
	struct tree *children[26];

	// boolen expression integer. 0 if false 1 if true
	//if the current stream is a word then it's 1 if not it is 0
	int endofword;
	// score of the word which get inclemented by one if a word is used again 
	int score;
};

// given nothing return a tree node (already malloced) the root/ nodes
struct tree *getnode(void)
{
	struct tree *curnode = NULL;

	curnode = (struct tree *)malloc(sizeof(struct tree));

	int i;

	curnode->endofword = 0;

	for(i = 0; i < 26; i++)
	{
		curnode->children[i] = NULL;
	}

	return curnode;
}

// insert a non exisiting node. If the node exist then add score of 1 to it
void insert(struct tree *x, const char *y, int a)
{
	int i;
	int ind;
	int length = strlen(y);

	struct tree *p = x;

	for(i = 0; i < length; i++)
	{
		ind = char_ind(y[i]);
		if(!p->children[ind])
		{
			p->children[ind] = getnode();
		}
		p = p->children[ind];
	}
	if(p->endofword)
	{
		p->score += 1;
		return;
	}
	p->endofword = 1;
	p->score = a;
	return;
}

int search(struct tree *x, const char *y)
{
	int i;
	int ind;
	int length = strlen(y);
	struct tree *p = x;

	for(i = 0; i < length; i++)
	{
		ind = char_ind(y[i]);

		if(!p->children[ind])
		{
			return 0;
		}
		p = p->children[ind];
	}

	if(p != NULL && p->endofword)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


// return 1 if all the children of the given node
// is NULL, if at lest one exist return 0
int endnode(struct tree *x)
{
	int i;
	for(i = 0; i < 26; i++)
	{
		if(x->children[i])
		{
			return 0;
		}
	}
	return 1;
}
// given a string and a character return a string with the exact same copy
// of the given string plus the character added at the end of it
char* add(char *x, char y)
{
	int i;
	int length = strlen(x);
	char *r = malloc(sizeof(char) * (length + 1));
	for(i = 0; i < length; i++)
	{
		r[i] = x[i];
	}
	r[length] = y;
	r[length + 1] = '\0';
	return r;
}

// given a string
// return a string with the last character deleted from it
void sub(char *x)
{
	int length = strlen(x);
	x[length-1] = '\0';
}

// finds and prints all the subtree of the given tree(technecally a string)
void restofword(struct tree *x, char *y, word * result)
{
	int i;
	struct tree *q = x;
	// if this is a word suggest it
	if(q->endofword)
	{
		if(q->score > result[0].score)
		{
			strcpy(result[1].w, result[0].w);
			result[1].score = result[0].score;
			strcpy(result[0].w, y);
			result[0].score = q->score;
		}
		else if(q->score > result[1].score)
		{
			strcpy(result[1].w, y);
			result[1].score = q->score;
		}
		// printf("%s\n", y);
	}
	// if not other children exists
	if(endnode(q))
	{
		return;
	}
	// go through the entire alphabet and 
	for(i = 0; i < 26; i++)
	{
		if(q->children[i])
		{
			y = add(y, 97 + i);

			restofword(q->children[i], y, result);

			sub(y);
		}
	}
}

//print all the things in the sub tree (for now)
int printit(struct tree *x, const char *y, word *result)
{
	struct tree *p = x;

	int i;
	int length = strlen(y);
	for(i = 0; i < length; i++)
	{
		int ind = char_ind(y[i]);

		if(!p->children[ind])
		{
			return 0;
		}

		p = p->children[ind];
	}

	// if the prefix is a word
	int a;
	if(p->endofword)
	{
		a = 1;
	}
	else
	{
		a = 0;
	}

	// if the prefix doesn't have any children 
	int b = endnode(p);

	if(a && b)
	{
		printf("%s\n", y);
		strcpy(result[0].w, y);
		return -1;
	}

	// if it isn't the last word

	if (!b)
	{
		char c[length];
		strcpy(c, y);
		restofword(p, c, result);
		return 1;
	}
}


// int main()
// {

// 	char keys[][8] = {"the", "a", "bigboot", "answer", "any", "by", "bye", "their"};

// 	char output[][32] = {"Not present in tree", "Present in tree"};

// 	struct tree *root = getnode();

// 	int i;
// 	int length = sizeof(keys) / sizeof(keys[0]);
// 	for(i = 0; i < length; i++)
// 	{
// 		insert(root, keys[i], 1);
// 	}
// 	int p = printit(root, "b");


// 	if(p == -1)
// 	{
// 		printf("no other strign with this string\n");
// 	}
// 	else if(p == 0)
// 	{
// 		printf("no string again yo\n");
// 	}
  
//     return 0; 
// }


int main()
{
	word *final_arr = get_ex_w_dic();
	// printmyword(final_arr);

	FILE *f;
	f = fopen(our_file, "r"); // read only

	// file not exisiting Error test case
	if(!f)
	{
		printf("Error! could not read file %s\n", our_file);
		exit(-1);
	}
	int lines;
	lines = countline(our_file);
	int a = countline(FILE_NAME);
	int *counter = &a;
	char q[30];
	// printf("%d\n", *counter);
	for(int i = 0; i < lines; i++)
	{
		fscanf(f, "%s", q);
		add_score(final_arr, counter, q);
	}
	fclose(f);

	struct tree *root = getnode();

	int i;


	// printmyword(final_arr);
	word * result = malloc(sizeof(word) * 2);

	for(i = 0; i < a -1; i++)
	{
		insert(root, final_arr[i].w, final_arr[i].score);
		// printf("the string is %s, the int is %d\n", final_arr[i].w, final_arr[i].score);
	}
	int p = printit(root, "a", result);

	printf("%s\n%s\n", result[0].w, result[1].w);

	if(p == -1)
	{
		printf("no other string with this string\n");
	}
	else if(p == 0)
	{
		printf("no string again yo\n");
	}
	free(result);
  
    return 0; 


}
