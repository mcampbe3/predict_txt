#include<stdio.h>
#include <termios.h>    // termios, TCSANOW, ECHO, ICANON
#include <unistd.h>     // STDIN_FILENO
#include <stdlib.h>


/**External Files**
// commented out for now until we get mains removed from other files and such
// include dictionaries as well?? txt files??
#include "treestructure.c"
#include "our_p_dict.c"
#include "make_p_tree.c"
#include "pull_in.c"

*****************/



// Small Graphics to explain to user on how to work the program
int intro()
{
  printf("Welcome to our Predictive Text Program\n");
  printf("Begin typing and if you see the word you \n");
  printf("are looking for press  |  1  |  2  |  3  | respectively.\n");
  printf("Hitting 'Enter' will exit the program. \n");
  printf("\n");
  printf("\n");

  return 0;
}

int main(void){   
    char c;   
    static struct termios oldt, newt;

    // tcgetattr gets the parameters of the current terminal
    // STDIN_FILENO will tell tcgetattr that it should write the settings
    // of stdin to oldt
    tcgetattr( STDIN_FILENO, &oldt);

    // now the settings will be copied
    newt = oldt;

    // ICANON normally takes care that one line at a time will be processed
    // that means it will return if it sees a "\n"
    newt.c_lflag &= ~(ICANON | ECHO | ECHOE);  


    // Those new settings will be set to STDIN
    // TCSANOW tells tcsetattr to change attributes immediately
    tcsetattr( STDIN_FILENO, TCSANOW, &newt);

    // need to create instance of the the tree here
    // fopen the dictionary files we want to read as arguments? I assume.
    // then go into the while loop and call our search funtion to check
    // the dictionaies for matching words and or phrases


    char arr[100];
    int i = 0;

    intro(); // calling intro function 

    printf("> ");
    // Pressing enter will quit this function and terminate the program
    while( (c = getchar()) != '\n')
    {

        // declaring this here to add ALL letters typed into this array
	arr[i] = c;
	i++;

	if(c == '1')
	{
		// chosen word in 1 spot
		// since we have an array of the word the user has partially
		// typed out we maybe can use these if statements to reset the array
		// to be empty each time they choose 1, 2 or 3
        	//putchar(c);
        	printf("\n");
        	printf("char in 1 spot here \n");
        	printf("> ");
        }
        else if(c == '2')
        {
        	// here would select chosen word in 2 spot
        	//putchar(c);
        	printf("\n");
        	printf("you pressed 2!\n");
        	printf("> ");
      	}
      	else if(c == '3')
      	{
        	// here would select chosen word in 3 spot
        	//putchar(c);
        	printf("\n");
        	printf("you pressed 3!\n");
        	printf("> ");
      	}
	else if(c == 127 || c == 8)
	{
		// this allows us to simulate 'backspace' in the program
		printf("\b");
		printf(" ");
		printf("\b");
	}
	else
	{
		// this will display what we want in the terminal one character at a time
        	putchar(c);
	}

    }

    // remove the last character unput by the user i.e. 1,2 or 3
    printf("\n");
    i--;
    arr[i] = 0x00;
    printf("%s\n", arr);

    // displays this when user hits the enter key
    printf("\n");
    printf("End of Predictive Text...\n");
    printf("Thanks For Playing...\n");
    printf("Exiting...\n");
    printf("\n");    

    // restore the old settings
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt);


    return 0;
}
