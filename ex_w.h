#ifndef EX_W_H_
#define EX_W_H_
#ifndef DEBUG
#define DEBUG 0
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define FILE_NAME "ex_w.txt"

// Word struct
typedef struct words
{
	char w[100];
	int score;
}word;

// Count lines in file
int countline(char *filename);

// Extract data to word pointer
word* extractdata(char *name);

// Print out word struct
void printmyword(word *x);

// Make sure our array is sorted
void sortarr(word* x);

// Make word dictionary
word* get_ex_w_dic();

#endif
