#include "ex_w.h"

int countline(char *filename)
{
	FILE *fp;
	fp = fopen(filename, "r");

	int ch;
	ch = 0;

	int lines;
	lines = 0;

	if(fp == NULL)
	{
		return 0;
	}
	//there's at least one line
	lines++;
	while((ch = fgetc(fp)) != EOF)
	{
		if(ch == '\n')
		{
			lines++;
		}
	}

	fclose(fp);
	return lines;
}
word* extractdata(char *name)
{
	FILE *filename;
	filename = fopen(name, "r"); // read only

	// file not exisiting Error test case
	if(! filename)
	{
		printf("Error! could not read file %s\n", FILE_NAME);
		exit(-1);
	}
	int lines;
	lines = countline(FILE_NAME);

	char q[100];

	word *val;
	val = malloc(sizeof(word) * lines);



	for(int i = 0; i < lines; i++)
	{
		word temp;

		fscanf(filename, "%s", q);
		strcpy(temp.w, q);
		temp.score = 1;
		val[i] = temp;
	}
	fclose(filename);
	return val;
}

// receive a string. either find the string and add 1 to it's value
// or not see the string and add it to our dictionary

void printmyword(word *x)
{
	int q;
	q = countline(FILE_NAME);

	for(int i = 0; i < q; i++)
	{
		printf("string is %s\n", x[i].w);
		printf("score is %d\n\n", x[i].score);
	}
	return;
}


//make sure our array is sorted
void sortarr(word* x)
{
	int n;
	char temp[100];
	n = countline(FILE_NAME);

	for(int i = 0; i < n - 1; i++)
	{
		for(int j = i + 1; j < n; j++)
		{
			if(strcmp(x[i].w, x[j].w) > 0)
			{
				strcpy(temp, x[i].w);
				strcpy(x[i].w, x[j].w);
				strcpy(x[j].w, temp);
			}
		}
	}
}

word* get_ex_w_dic()
{
	word *final_arr;
	int p;
	p = countline(FILE_NAME);
	final_arr = malloc(sizeof(word) * p);

	final_arr = extractdata(FILE_NAME);
	sortarr(final_arr);

	//printmyword(final_arr);

	return final_arr;
}
