#ifndef PULL_IN_P_H_
#define PULL_IN_P_H_
#ifndef DEBUG
#define DEBUG 0
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// Get number of lines in file
int get_num_lines(FILE *file);

// Get number of characters in file
int get_char(FILE *file);

// Clean up file for phrase dictionary
void clean_up_file(const char *one, const char *two);

#endif
