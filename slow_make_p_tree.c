#include "slow_make_p_tree.h"

// Make test array
Dict_part *make_test_dict()
{
	// Make first Dict_part phrase
	Dict_part *one = (Dict_part *)malloc(sizeof(Dict_part));
	one->phrase[0] = "this";
	one->phrase[1] = "is";
	one->phrase[2] = "a";
	one->phrase[3] = "list";
	one->phrase[4] = "of";
	one->phrase[5] = "words";
	one->score = 3;

	// Make second Dict_part phrase
	Dict_part *two = (Dict_part *)malloc(sizeof(Dict_part));
	two->phrase[0] = "maybe";
	two->phrase[1] = "this";
	two->phrase[2] = "work";
	two->score = 2;

	// Make third Dict_part phrase
	Dict_part *three = (Dict_part *)malloc(sizeof(Dict_part));
	three->phrase[0] = "maybe";
	three->phrase[1] = "it";
	three->phrase[2] = "does";
	three->score = 2;

	// Make fourth Dict_part phrase
	Dict_part *four = (Dict_part *)malloc(sizeof(Dict_part));
	four->phrase[0] = "maybe";
	four->phrase[1] = "it";
	four->phrase[2] = "doesn't";
	four->score = 1;

	// Make fifth Dict_part phrase
	Dict_part *five = (Dict_part *)malloc(sizeof(Dict_part));
	five->phrase[0] = "i";
	five->phrase[1] = "guess";
	five->phrase[2] = "we";
	five->phrase[3] = "shall";
	five->phrase[4] = "see";
	five->score = 1;

	// Make sixth Dict_part phrase
	Dict_part *six = (Dict_part *)malloc(sizeof(Dict_part));
	six->phrase[0] = "here";
	six->phrase[1] = "goes";
	six->phrase[2] = "nothing";
	six->score = 1;

	// Add Dict_parts to arr dictionary
	Dict_part *arr = (Dict_part *)malloc(sizeof(Dict_part) * 6);
	arr[0] = *one;
	arr[1] = *two;
	arr[2] = *three;
	arr[3] = *four;
	arr[4] = *five;
	arr[5] = *six;

	// Return
	return arr;
}

Dict_part *find_sentence(Dict_part *dict, char **input_array, int count, int len)
{
	Dict_part *new_arr = malloc(sizeof(Dict_part) * len);
	int new_count = 0;
	for (int i = 0; i < len; i++)
	{
		if (dict[i].phrase[count] == input_array[count])
		{
			new_arr[new_count] = dict[i];
			new_count++;
		}
	}
	return new_arr;
}

char **make_test_input()
{
	char **input_word = (char **)malloc(sizeof(char *) * num);
	input_word[0] = "maybe";
	input_word[1] = "it";
	return input_word;
}

Dict_part *search(Dict_part *arr, char **input_word, int len)
{
	int count = 0;
	for (int i = 0; i < num; i++)
	{
		arr = find_sentence(arr, input_word, count, len);
		count++;
	}
	return arr;
}

void print_predict(Dict_part *arr)
{
	for (int j = 0; j < 2; j++)
	{
		printf("%s %d\n", arr[j].phrase[num], arr[j].score);
	}
}

Dict_part *slow_tree_search(Dict_part *arr, char *input)
{
	// Make test dictionary
	// Dict_part *arr = make_test_dict();

	//Test number of of input words
	// int num = 2;

	// Test input word
	char **input_word = make_test_input();

	//Test # of lines in file
	int len = 6;

	// Get most used words that match phrase
	arr = search(arr, input_word, len);

	// Print results
	print_predict(arr);

	return arr;
}
