#include "our_w_dict.h"

/* this file creates our users word of dictionaries which consistes of
the external dictionary of list of words with score of 1 and possible
words that the user uses that is not in the external dictionary will be added to this dictionary
*/

//create a word type with score one given a string
word make_word(char *x)
{
	word temp;
	// temp.score = malloc(sizeof(int));
	// temp.w = malloc(sizeof(char) * strlen(x));
	temp.score = 1;
	strcpy(temp.w, x);
	return temp;
}

void addonesize(word *before, int *count)
{
	word *temp;
	temp = malloc(sizeof(word) * (*count + 1));
	for(int i = 0; i < *count; i++)
	{
		strcpy(temp[i].w, before[i].w);
		temp[i].score = before[i].score;
	}
	sortarr(temp);
	before = temp;
	free(temp);
}

// if a word doesn't exist then add the word to the final array
word *add_new_word(word *final_arr, int *count, char *x)
{
	word temp = make_word(x);
	addonesize(final_arr, count);
	count++;
	final_arr[*count] = temp;
	return final_arr;
}


// given our final array and the # of items in that array, either find the same word
// add a score of 1 to its score, or if it's not found then add it to the final array and sort it
void add_score(word *final_arr, int *count, char *x)
{
	//for all the elements in the array
	for(int i = 0; i < *count; i++)
	{
		// if found
		if(strcmp(final_arr[i].w, x) == 0)
		{
			// add one to its score
			final_arr[i].score += 1;
			return;
		}
	}
	// if not found we get here
	final_arr = add_new_word(final_arr, count, x);
	//printf("ooh nooooooooo\n\n\n");
	return;

}
/*
int main()
{
	word *final_arr = get_ex_w_dic();
	//printmyword(final_arr);

	FILE *f;
	f = fopen(our_file, "r"); // read only

	// file not exisiting Error test case
	if(! f)
	{
		printf("Error! could not read file %s\n", our_file);
		exit(-1);
	}
	int lines;
	lines = countline(our_file);
	int a = countline(FILE_NAME);
	int *counter = &a;
	char q[100];
	printf("%d\n", *counter);
	for(int i = 0; i < lines; i++)
	{
		//printf("hi\n");
		fscanf(f, "%s", q);
		//printf("%s\n\n", q);
		add_score(final_arr, counter, q);
	}
	printmyword(final_arr);
	fclose(f);

}*/
