#include "pull_in_p.h"

// Get number of lines in file
int get_num_lines(FILE *file)
{
	int num_lines = 0;
	char c = getc(file);
	while((c=getc(file)) != EOF)
	{
		if (c == '\n')
		{
			num_lines++;
		}
	}
	fseek(file, 0, SEEK_SET);
	return num_lines;
}

// Get number of characters in file
int get_char(FILE *file)
{
	fseek(file, 0, SEEK_END);
	int num_chars = ftell(file);
	fseek(file, 0, SEEK_SET);
	return num_chars;
}

// Clean up file for phrase dictionary
void clean_up_file(const char *one, const char *two)
{
	// Initialize variables
	char c;
	int match = 0;
	int appended;
	char *alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz -',";

	// Open first file to read
	FILE *f;
	f = fopen(one, "r");

	// Get number of lines in file
	int lines = get_num_lines(f);

	// Get number of characters in file
	int char_in_file = get_char(f);

	// Open second file to write
	FILE *f2;
	f2 = fopen(two, "w");

	// Clean up the file
	for (int i = 0; i < lines; i++)
	{
		// Initialize variables
		char *line;
		appended = 0;
		int cut_off = 0;

		// Get each line in the file
		fgets(line, char_in_file, f);

		// Get and clean each line in file
		if (i > 514 && i < 33000 && strlen(line) > 3 && line[1] != '\n' && line[0] != ' ')
		{
			for (int j = 0; j < strlen(line); j++)
			{
				cut_off = 0;
				match = 0;
				for (int k = 0; k < strlen(alpha); k++)
				{
					if (line[j] == alpha[k])
					{
						match = 1;
						break;
					}
					else if (k == strlen(alpha) - 1 && match == 0)
					{
						char *tmp = (char *)malloc(sizeof(char) * j);
						strncpy(tmp, line, j);
						memset(line, 0, strlen(line));
						strcpy(line, tmp);
						cut_off = 1;
					}
				}
				if (cut_off)
				{
					break;
				}
			}

			// Write cleaned up lines to second file
			if (strcmp(line, "SECTION") != 0)
			{
				fprintf(f2, "%s\n", line);
			}
		}
	}

	// Close files
	fclose(f);
	fclose(f2);
}
