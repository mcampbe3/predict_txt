#ifndef SLOW_MAKE_P_TREE_H_
#define SLOW_MAKE_P_TREE_H_
#ifndef DEBUG
#define DEBUG 0
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Dict_parts
{
	char phrase[1000][1000];
	int score;
} Dict_part;

// Make a test dictionary
Dict_part *make_test_dict();

// Find the sentence in the tree
Dict_part *find_sentence(Dict_part *dict, char **input_array, int count, int len);

// Make user input test
char **make_test_input();

// Search for user input in tree
Dict_part *search(Dict_part *arr, char **input_word, int len);

// Print words user would most likely use next
void print_predict(Dict_part *arr);

// Search the tree
Dict_part *slow_tree_search(Dict_part *arr, char *input);

#endif
