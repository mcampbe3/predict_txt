#ifndef OUR_P_DICT_H_
#define OUR_P_DICT_H_
#ifndef DEBUG
#define DEBUG 0
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Items in dictionary struct
typedef struct Dict_parts
{
	int score;
	char phrase[1000][1000];
} Dict_part;

// Get the length of a file
int len_of_file(FILE *file);

// Get the number of lines in a file
int lines_in_file(FILE *file);

// Get the number of words in each line of a file
int *num_words_in_lines(FILE *file, int line_num);

// Initialize dictionary
Dict_part **init_dict(int line_num, int *lens);

// Put contents of file into dictionary
void load_phrases(FILE *file, Dict_part **dictionary, int length, int line_num, int *lens);

// Print dictionary
void print_dict(Dict_part **dictionary, int line_num, int *lens);

// Make dictionary
Dict_part **make_dic(const char *one);

#endif
